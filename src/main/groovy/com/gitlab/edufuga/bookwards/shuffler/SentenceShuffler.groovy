package com.gitlab.edufuga.bookwards.shuffler

import com.gitlab.edufuga.bookwards.messageprinter.MessagePrinter
import com.gitlab.edufuga.wordlines.core.WordStatusDate
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader
import com.gitlab.edufuga.wordlines.recordreader.RecordReader
import com.gitlab.edufuga.wordlines.sentenceparser.SentenceParser

import java.util.function.Supplier

class SentenceShuffler {
    private final SentenceParser sentenceParser
    private final MessagePrinter messagePrinter

    SentenceShuffler(SentenceParser sentenceParser, MessagePrinter messagePrinter) {
        this.sentenceParser = sentenceParser
        this.messagePrinter = messagePrinter
    }

    // Usable without injecting the records from outside and without giving the concrete word ("pivot")
    void shuffle(String sentence) {
        List<WordStatusDate> wordsInSentence = sentenceParser.parseSentenceIntoRecords (sentence)
        // This assumes the word from the record (file system) equals the word from the sentence
        String word = wordsInSentence.first().getWord()
        shuffle(sentence, wordsInSentence, word, false)
    }

    // Usable without injecting the records from outside.
    void shuffle(String sentence, String word, boolean hide=false) {
        List<WordStatusDate> wordsInSentence = sentenceParser.parseSentenceIntoRecords (sentence)
        shuffle(sentence, wordsInSentence, word, hide)
    }

    void shuffle(String sentence, List<WordStatusDate> wordsInSentence, String word, boolean eol=false,
                 boolean color=true, boolean hide=false, boolean underline=false) {
        // Rewrite the shuffled sentence in the right order. (Ideally this should work for multi-word checks).
        // FIXME: Not enough information in the current objects to swap elements of the sentence AND keep
        // the punctuation.
        // TODO: Or maybe it's easy: Just swap (replace) two WordStatusDate (records). Repeat several times.
        // Shuffle the wordsInSentence and then choose some of them.

        // If $word is contained only ONCE in the sentence, the word may be swapped with other words repeatedly.
        List<String> allWordInSentenceMatches = sentence.findAll(word)
        if (allWordInSentenceMatches != null || allWordInSentenceMatches.size() == 1) {
            // Swap the $word with all the shuffled records in the sentence.

            String shuffledSentence = new String(sentence)
            List<WordStatusDate> shuffledRecords = new ArrayList<>()
            shuffledRecords.addAll(wordsInSentence)
            Collections.shuffle(shuffledRecords)
            for (WordStatusDate recordWithWordToReplace : shuffledRecords) {
                String wordToReplace = recordWithWordToReplace.getWord()
                if (!sentence.contains(wordToReplace)) {
                    continue
                }
                if (wordToReplace.equals(word)) {
                    continue
                }
                List<String> allWordToReplaceInSentenceMatches = sentence.findAll(wordToReplace)
                if (allWordToReplaceInSentenceMatches == null || allWordToReplaceInSentenceMatches.size() != 1) {
                    continue
                }

                // There is only *one* match of the $wordToReplace in the $sentence. The word can be switched with
                // $word. (This assumes that $word is also unique, which it doesn't have to be true.)

                // Replace wordToReplace with $word (they are different words)
                // TODO: How to do the wordToReplace <-> word shuffling?
                // Trick: Use UPPERCASE to differentiate the replacements. This is an UGLY HACK.
                // ACHTUNG: Dies does not consider SPACES nor WORDS that are "contained" within other WORDS (repetitions).
                // if "ich" is contained inside "mich" (which appears first), then this leads to a wrong replacement.
                // So we DO need to know about word delimitations in order to switch them properly.
                String temporaryReplacementForWord = word.toUpperCase()
                String temporaryReplacementForWordToReplace = wordToReplace.toUpperCase()
                shuffledSentence = shuffledSentence.replace(wordToReplace, temporaryReplacementForWordToReplace)
                shuffledSentence = shuffledSentence.replace(word, temporaryReplacementForWord)
                shuffledSentence = shuffledSentence.replace(temporaryReplacementForWordToReplace, word)
                shuffledSentence = shuffledSentence.replace(temporaryReplacementForWord, wordToReplace)
            }

            if (shuffledSentence.size() != sentence.size()) {
                // Irgendwas hat nicht funktioniert.
                shuffledSentence = sentence
            }

            Supplier<Map<Integer, Boolean>> shuffledSentenceUnderliner = () -> Collections.emptyMap()
            if (underline) {
                shuffledSentenceUnderliner = () -> messagePrinter.getCharactersToUnderline (shuffledSentence, word)
            }
            messagePrinter.printMessage(shuffledSentence, shuffledRecords, shuffledSentenceUnderliner, eol, color, hide)
        }
    }

    static void main(String[] args) {
        if(args.size()!=2) {
            println "Give me the status folder and the sentence."
            return
        }

        String statusFolder = args[0]
        String sentence = args[1]

        RecordReader recordReader = new FileSystemRecordReader (statusFolder)
        SentenceParser sentenceParser = new SentenceParser(recordReader)
        MessagePrinter messagePrinter = new MessagePrinter()

        SentenceShuffler sentenceShuffler = new SentenceShuffler(sentenceParser, messagePrinter)
        sentenceShuffler.shuffle(sentence)
    }
}